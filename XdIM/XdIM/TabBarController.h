//
//  TabBarController.h
//  XdIM
//
//  Created by Dyw on 2018/11/12.
//  Copyright © 2018 Dyw. All rights reserved.
//

#import <UIKit/UIKit.h>

#define SBID_TabBarController @"TabBarController"

NS_ASSUME_NONNULL_BEGIN

@interface TabBarController : UITabBarController

+ (instancetype)creat;

@end

NS_ASSUME_NONNULL_END
