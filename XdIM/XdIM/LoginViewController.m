//
//  LoginViewController.m
//  XdIM
//
//  Created by Dyw on 2018/11/12.
//  Copyright © 2018 Dyw. All rights reserved.
//

#import "LoginViewController.h"
#import "ImEventManager.h"
#import "ToastUtils.h"
#import "AppMacros.h"

@interface LoginViewController ()
@property (weak, nonatomic) IBOutlet UITextField *nameTF;
@property (weak, nonatomic) IBOutlet UITextField *pwdTF;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (IBAction)loginButtonClic:(id)sender {
    [ToastUtils showHUD];
    [ImEventManagerObj loginIMUserName:self.nameTF.text pwd:self.pwdTF.text success:^{
        [ToastUtils hiddHUD];
        [ToastUtils showToast:@"登录成功"];
        
    } fail:^(int code, NSString * _Nonnull err) {
        [ToastUtils hiddHUD];
        [ToastUtils showToast:@"登录失败"];
    }];
    
}


@end
