//
//  ImEventManager.m
//  XdIM
//
//  Created by Dyw on 2018/11/12.
//  Copyright © 2018 Dyw. All rights reserved.
//

#import "ImEventManager.h"
#import <IMMessageExt/IMMessageExt.h>


#define IM_APPID @"1400035433"

@interface ImEventManager ()
<TIMConnListener, TIMUserStatusListener, TIMRefreshListener, TIMMessageRevokeListener, TIMFriendshipListener>


@end

@implementation ImEventManager
static ImEventManager *_imEventManager;

#pragma mark - 单利
+ (instancetype)manager{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _imEventManager = [[ImEventManager alloc] init];
    });
    return _imEventManager;
}

+(instancetype)allocWithZone:(struct _NSZone *)zone{
    if(!_imEventManager){
        _imEventManager = [super allocWithZone:zone];
    }
    return _imEventManager;
}

-(id)mutableCopy{
    return _imEventManager;
}


/* 初始化IM */
- (void)initIM{
    TIMManager *manager = [TIMManager sharedInstance];
    TIMSdkConfig *config = [[TIMSdkConfig alloc] init];
    config.sdkAppId = IM_APPID.intValue;
    config.accountType = @"4763";
    config.disableCrashReport = NO;
    config.connListener = self;
    [manager initSdk:config];
    
    TIMUserConfig *userConfig = [[TIMUserConfig alloc] init];
    userConfig.disableStorage = YES;//禁用本地存储（加载消息扩展包有效）
    userConfig.disableAutoReport = YES;//禁止自动上报（加载消息扩展包有效）
    userConfig.enableReadReceipt = YES;//开启C2C已读回执（加载消息扩展包有效）
    userConfig.disableRecnetContact = NO;//不开启最近联系人（加载消息扩展包有效）
    userConfig.disableRecentContactNotify = YES;//不通过onNewMessage:抛出最新联系人的最后一条消息（加载消息扩展包有效）
    userConfig.enableFriendshipProxy = YES;//开启关系链数据本地缓存功能（加载好友扩展包有效）
    userConfig.enableGroupAssistant = YES;//开启群组数据本地缓存功能（加载群组扩展包有效）
    
    TIMFriendProfileOption *fpOption = [[TIMFriendProfileOption alloc] init];
    fpOption.friendFlags = 0xffffff;//需要获取的好友信息标志（TIMProfileFlag）,默认为0xffffff
    fpOption.friendCustom = nil;//需要获取的好友自定义信息（NSString*）列表
    fpOption.userCustom = nil;//需要获取的用户自定义信息（NSString*）列表
    userConfig.friendProfileOpt = fpOption;//设置默认拉取的好友资料
    userConfig.userStatusListener = self;//用户登录状态监听器
    userConfig.refreshListener = self;//会话刷新监听器（未读计数、已读同步）（加载消息扩展包有效）
    //    userConfig.receiptListener = self;//消息已读回执监听器（加载消息扩展包有效）
    //    userConfig.messageUpdateListener = self;//消息svr重写监听器（加载消息扩展包有效）
    //    userConfig.uploadProgressListener = self;//文件上传进度监听器
    //    userConfig.groupEventListener todo
    userConfig.messgeRevokeListener = self;
    userConfig.friendshipListener = self;//关系链数据本地缓存监听器（加载好友扩展包、enableFriendshipProxy有效）
    [manager setUserConfig:userConfig];
}

/* 登录IM */
- (void)loginIMUserName:(NSString *)name pwd:(NSString *)pwd success:(void(^)(void))successBlock fail:(void(^)(int code, NSString *err))failBlock{
    TIMLoginParam *loginParam = [[TIMLoginParam alloc] init];
    loginParam.identifier = name;
    loginParam.userSig = pwd;
    loginParam.appidAt3rd = IM_APPID;
    
    [[TIMManager sharedInstance] login:loginParam succ:^{
        NSLog(@"登录成功");
        !self.loginSuccessBlock?:self.loginSuccessBlock();
        !successBlock?:successBlock();
    } fail:^(int code, NSString *msg) {
        NSLog(@"登录失败->%d ->%@", code, msg);
        !failBlock?:failBlock(code, msg);
    }];
}

#pragma mark - TIMConnListener
/**
 *  网络连接成功
 */
- (void)onConnSucc{
    NSLog(@"网络连接成功");
}

/**
 *  网络连接失败
 *
 *  @param code 错误码
 *  @param err  错误描述
 */
- (void)onConnFailed:(int)code err:(NSString*)err{
    NSLog(@"网络连接失败->%d  ->%@", code, err);
}

/**
 *  网络连接断开（断线只是通知用户，不需要重新登陆，重连以后会自动上线）
 *
 *  @param code 错误码
 *  @param err  错误描述
 */
- (void)onDisconnect:(int)code err:(NSString*)err{
    NSLog(@"网络连接断开->%d  ->%@", code, err);
}


/**
 *  连接中
 */
- (void)onConnecting{
    NSLog(@"连接中");
}

#pragma mark - TIMUserStatusListener
/**
 *  踢下线通知
 */
- (void)onForceOffline{
    NSLog(@"踢下线通知");
}

/**
 *  断线重连失败
 */
- (void)onReConnFailed:(int)code err:(NSString*)err{
    NSLog(@"断线重连失败->%d  ->%@", code, err);
}

/**
 *  用户登录的userSig过期（用户需要重新获取userSig后登录）
 */
- (void)onUserSigExpired{
    NSLog(@"用户登录的userSig过期");
}

#pragma mark - TIMRefreshListener
/**
 *  刷新会话
 */
- (void)onRefresh{
    !self.refreshConversationListBlock?:self.refreshConversationListBlock();
    NSLog(@"刷新会话");
}

/**
 *  刷新部分会话（包括多终端已读上报同步）
 *
 *  @param conversations 会话（TIMConversation*）列表
 */
- (void)onRefreshConversations:(NSArray*)conversations{
    NSLog(@"刷新部分会话->%@", conversations);
}

#pragma mark - TIMMessageRevokeListener
/**
 *  消息撤回通知
 *
 *  @param locator 被撤回消息的标识
 */
- (void)onRevokeMessage:(TIMMessageLocator*)locator{
    NSLog(@"消息撤回通知->%@", locator);
}

#pragma mark - TIMFriendshipListener
/**
 *  添加好友通知
 *
 *  @param users 好友列表（TIMUserProfile*）
 */
- (void)onAddFriends:(NSArray*)users{
    NSLog(@"添加好友通知->%@", users);
}

/**
 *  删除好友通知
 *
 *  @param identifiers 用户id列表（NSString*）
 */
- (void)onDelFriends:(NSArray*)identifiers{
    NSLog(@"删除好友通知->%@", identifiers);
}

/**
 *  好友资料更新通知
 *
 *  @param profiles 资料列表（TIMUserProfile*）
 */
- (void)onFriendProfileUpdate:(NSArray*)profiles{
    NSLog(@"友资料更新通知->%@", profiles);
}

/**
 *  好友申请通知
 *
 *  @param reqs 好友申请者id列表（TIMSNSChangeInfo*）
 */
- (void)onAddFriendReqs:(NSArray*)reqs{
    NSLog(@"好友申请通知->%@", reqs);
}

@end
