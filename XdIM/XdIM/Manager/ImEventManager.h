//
//  ImEventManager.h
//  XdIM
//
//  Created by Dyw on 2018/11/12.
//  Copyright © 2018 Dyw. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ImSDK/ImSDK.h>

#define ImEventManagerObj [ImEventManager manager]

NS_ASSUME_NONNULL_BEGIN

@interface ImEventManager : NSObject
/* 登录成功回调 */
@property (nonatomic, copy) void(^loginSuccessBlock)(void);
/* 刷新会话回调 */
@property (nonatomic, copy) void(^refreshConversationListBlock)(void);

/* 单利创建 */
+ (instancetype)manager;

/* 初始化IM */
- (void)initIM;

/* 登录IM */
- (void)loginIMUserName:(NSString *)name pwd:(NSString *)pwd success:(void(^)(void))successBlock fail:(void(^)(int code, NSString *err))failBlock;

@end

NS_ASSUME_NONNULL_END
