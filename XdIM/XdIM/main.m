//
//  main.m
//  XdIM
//
//  Created by Dyw on 2018/11/12.
//  Copyright © 2018 Dyw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
