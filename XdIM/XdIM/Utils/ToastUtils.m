//
//  ToastUtils.m
//  AmMall
//
//  Created by Dyl on 2018/10/29.
//  Copyright © 2018 Dyl. All rights reserved.
//

#import "ToastUtils.h"
#import "UIView+Toast.h"
#import "AppMacros.h"

@interface ToastUtils ()

@property (nonatomic, strong) UIView *backView;
@property (nonatomic, strong) UIImageView *animateImageView;

@end

@implementation ToastUtils
static ToastUtils *_toastUtils;

/* 单利创建 */
+ (instancetype)share{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _toastUtils = [[ToastUtils alloc] init];
    });
    return _toastUtils;
}

+ (instancetype)allocWithZone:(struct _NSZone *)zone{
    if(!_toastUtils){
        _toastUtils = [super allocWithZone:zone];
    }
    return _toastUtils;
}

- (id)mutableCopy{
    return _toastUtils;
}

/** 弹出一个展示2秒的文字提示 */
+ (void)showToast:(NSString *)text{
    [KEYWINDOW hideAllToasts];
    CSToastStyle *style = [[CSToastStyle alloc] initWithDefaultStyle];
    style.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.8];
    style.cornerRadius = 5.0f;
    style.verticalPadding = 15;
    [KEYWINDOW makeToast:text duration:2.0f position:CSToastPositionCenter style:style];
}

+ (void)showHUD{
    [KEYWINDOW makeToastActivity:CSToastPositionCenter];
}

+ (void)hiddHUD{
    [KEYWINDOW hideToastActivity];
}

+ (void)showAxmHudBack:(BOOL)isBack{
    [CSToastManager sharedStyle].verticalPadding = 0;
    [CSToastManager setTapToDismissEnabled:NO];
    ToastUtilsObj.backView.frame = CGRectMake(0, isBack?NAV_HEIGHT:0, SCREEN_WIDTH, SCREEN_HEIGHT-(isBack?NAV_HEIGHT:0));
    ToastUtilsObj.animateImageView.center = CGPointMake(SCREEN_WIDTH/2.0f, SCREEN_HEIGHT/2.0f-(isBack?NAV_HEIGHT/2.0f:0));
    [KEYWINDOW showToast:ToastUtilsObj.backView duration:NSIntegerMax position:CSToastPositionBottom completion:nil];
    [ToastUtilsObj.animateImageView startAnimating];
}

+ (void)hiddAxmHUD{
    [KEYWINDOW hideToast:ToastUtilsObj.backView];
    [ToastUtilsObj.animateImageView stopAnimating];
}

- (UIImageView *)animateImageView{
    if(!_animateImageView){
        _animateImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 80, 80)];
        NSMutableArray <UIImage *>* imagesArray = [NSMutableArray array];
        for (int i=0; i<3; i++) {
            NSString *imageName = [NSString stringWithFormat:@"hud_axm_%d", i];
            [imagesArray addObject:ImageNamed(imageName)];
        }
        _animateImageView.animationImages = imagesArray;
        _animateImageView.animationRepeatCount = 0;
        _animateImageView.animationDuration = 0.3;
    }
    return _animateImageView;
}

- (UIView *)backView{
    if(!_backView){
        _backView = [[UIView alloc] initWithFrame:SCREEN_BOUNDS];
        _backView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.3];
        [_backView addSubview:self.animateImageView];
        self.animateImageView.center = _backView.center;
    }
    return _backView;
}

@end
