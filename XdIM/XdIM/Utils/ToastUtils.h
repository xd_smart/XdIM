//
//  ToastUtils.h
//  AmMall
//
//  Created by Dyl on 2018/10/29.
//  Copyright © 2018 Dyl. All rights reserved.
//

#import <Foundation/Foundation.h>

#define ToastUtilsObj [ToastUtils share]

NS_ASSUME_NONNULL_BEGIN

@interface ToastUtils : NSObject

/* 单利创建 */
+ (instancetype)share;

/** 弹出一个展示2秒的文字提示 */
+ (void)showToast:(NSString *)text;

+ (void)showHUD;

+ (void)hiddHUD;

+ (void)showAxmHudBack:(BOOL)isBack;

+ (void)hiddAxmHUD;

@end

NS_ASSUME_NONNULL_END
