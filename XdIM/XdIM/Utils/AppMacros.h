//
//  AppMacros.h
//  AmMall
//
//  Created by Dyl on 2018/10/29.
//  Copyright © 2018 Dyl. All rights reserved.
//

#ifndef AppMacros_h
#define AppMacros_h

//---------日志相关------------------------
// 使用xcode控制台打印: 注释下一行
//#define SET_NSLOGGER 1

#ifdef SET_NSLOGGER
#import "NSLogger.h"
#else

#ifdef DEBUG
#define LoggerError(level,...) NSLog(__VA_ARGS__)
#define LoggerApp(level, ...) NSLog(__VA_ARGS__)
#define LoggerView(level,...) NSLog(__VA_ARGS__)
#define LoggerService(level,...) NSLog(__VA_ARGS__)
#define LoggerModel(level,...) NSLog(__VA_ARGS__)
#define LoggerData(level,...) NSLog(__VA_ARGS__)
#define LoggerNetwork(level,...) NSLog(__VA_ARGS__)
#define LoggerLocation(level,...) NSLog(__VA_ARGS__)
#define LoggerPush(level,...) NSLog(__VA_ARGS__)
#define LoggerFile(level,...) NSLog(__VA_ARGS__)
#define LoggerSharing(level,...) NSLog(__VA_ARGS__)
#define LoggerAd(level,...) NSLog(__VA_ARGS__)
#else
#define NSLog(...)                      while(0) {}
#define LoggerError(...)                while(0) {}
#define LoggerApp(level, ...)           while(0) {}
#define LoggerView(...)                 while(0) {}
#define LoggerService(...)              while(0) {}
#define LoggerModel(...)                while(0) {}
#define LoggerData(...)                 while(0) {}
#define LoggerNetwork(...)              while(0) {}
#define LoggerLocation(...)             while(0) {}
#define LoggerPush(...)                 while(0) {}
#define LoggerFile(...)                 while(0) {}
#define LoggerSharing(...)              while(0) {}
#define LoggerAd(...)                   while(0) {}
#endif

#endif
//---------日志相关-------------------------

//全局的Window
#define KEYWINDOW [UIApplication sharedApplication].keyWindow

//-------------------获取设备大小-------------------------
//获取屏幕 宽度、高度
#define SCREEN_BOUNDS [UIScreen mainScreen].bounds
#define SCREEN_WIDTH (SCREEN_BOUNDS.size.width)
#define SCREEN_HEIGHT (SCREEN_BOUNDS.size.height)
/* 是否是iphoneX */
#define ISIPHONEX (((SCREEN_WIDTH == 812.0f || SCREEN_WIDTH == 896.0f)?YES:NO))
/* 导航栏高度 */
#define NAV_HEIGHT (ISIPHONEX?88.0f:64.0f)
/* 底部Tabbar高度 */
#define TABBAR_HEIGHT (ISIPHONEX?83.0f:49.0f)
//-------------------获取设备大小-------------------------

//----------------------图片----------------------------
//建议使用前两种宏定义,性能高于后者
//读取本地图片
#define LOADIMAGE(file,ext) [UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:file ofType:ext]]
//定义UIImage对象
#define IMAGE(A) [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:A ofType:nil]]
//定义UIImage对象
#define ImageNamed(name) [UIImage imageNamed:name]
//----------------------图片----------------------------

//----------------------颜色工具---------------------------
// rgb颜色转换（16进制->10进制）
#define ColorFromHEX(value) [UIColor colorWithRed:((float)((value & 0xFF0000) >> 16))/255.0 green:((float)((value & 0xFF00) >> 8))/255.0 blue:((float)(value & 0xFF))/255.0 alpha:1.0]
//带有RGBA的颜色设置
#define COLOR(R, G, B, A) [UIColor colorWithRed:R/255.0 green:G/255.0 blue:B/255.0 alpha:A]
// 获取RGB颜色
#define RGBA(r,g,b,a) [UIColor colorWithRed:r/255.0f green:g/255.0f blue:b/255.0f alpha:a]
#define RGB(r,g,b) RGBA(r,g,b,1.0f)
//----------------------颜色工具---------------------------

//----------------------系统----------------------------
//获取系统版本
#define IOS_VERSION [[[UIDevice currentDevice] systemVersion] floatValue]
#define CurrentSystemVersion [[UIDevice currentDevice] systemVersion]
//----------------------系统----------------------------

//---------------------资源读取-------------------------
//xib cell
#define NIBCELL(cellID) [tableView dequeueReusableCellWithIdentifier:cellID]; if(!cell){ cell = [[NSBundle mainBundle] loadNibNamed:cellID owner:self options:nil].lastObject; }
#define NibCell_WithName(cellID,cellName) [tableView dequeueReusableCellWithIdentifier:cellID]; if(!cellName){ cellName = [[NSBundle mainBundle] loadNibNamed:cellID owner:self options:nil].lastObject; }
//xib view
#define NIBVIEW(viewClass) [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([viewClass class]) owner:self options:nil].lastObject
//读plis 数组
#define PLIS_GET_ARR(nameStr) [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:nameStr ofType:@"plist"]]
//读plis 字典
#define PLIS_GET_DIC(nameStr) [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:nameStr ofType:@"plist"]]
//---------------------资源读取-------------------------

#endif /* AppMacros_h */
