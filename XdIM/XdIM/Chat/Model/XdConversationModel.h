//
//  XdConversationModel.h
//  XdIM
//
//  Created by Dyw on 2018/11/13.
//  Copyright © 2018 Dyw. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ImSDK/ImSDK.h>

NS_ASSUME_NONNULL_BEGIN

@interface XdConversationModel : NSObject

@property (nonatomic, strong) TIMConversation *tIMConversation;
@property (nonatomic, strong) TIMUserProfile *tIMUserProfile;;

@end

NS_ASSUME_NONNULL_END
