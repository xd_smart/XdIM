//
//  ChatListViewController.m
//  XdIM
//
//  Created by Dyw on 2018/11/12.
//  Copyright © 2018 Dyw. All rights reserved.
//

#import "ChatListViewController.h"
#import <ImSDK/ImSDK.h>
#import <IMMessageExt/IMMessageExt.h>
#import "ImEventManager.h"
#import "ChatListCell.h"
#import "ChatViewController.h"

@interface ChatListViewController ()
<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray <XdConversationModel *>* dataSuorce;

@end

@implementation ChatListViewController

#pragma mark - life cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.view addSubview:self.tableView];
    /* 刷新会话回调 */
    ImEventManagerObj.refreshConversationListBlock = ^{
        TIMManager *tIMManager = [TIMManager sharedInstance];
        NSArray <TIMConversation *>* tIMConversationsArr = [tIMManager getConversationList];
        
        NSMutableArray <NSString *>*userIdsArr = [NSMutableArray array];
        [tIMConversationsArr enumerateObjectsUsingBlock:^(TIMConversation * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            [userIdsArr addObject:[obj getReceiver]];
            XdConversationModel *xdConversationModel = [XdConversationModel new];
            xdConversationModel.tIMConversation = obj;
            [self.dataSuorce addObject:xdConversationModel];
        }];
        
        [[TIMFriendshipManager sharedInstance] getUsersProfile:userIdsArr succ:^(NSArray <TIMUserProfile *>*friends) {
           
            [friends enumerateObjectsUsingBlock:^(TIMUserProfile * _Nonnull userProfile, NSUInteger idx, BOOL * _Nonnull stop) {
                [self.dataSuorce enumerateObjectsUsingBlock:^(XdConversationModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    if([userProfile.identifier isEqualToString:[obj.tIMConversation getReceiver]]){
                        obj.tIMUserProfile = userProfile;
                    }
                }];
            }];
            
            NSLog(@"-->%@", friends);
            
            [self.tableView reloadData];
            NSLog(@"会话列表--》%@", self.dataSuorce);

        } fail:^(int code, NSString *msg) {
            
        }];
    };
}

#pragma mark - public methods
#pragma mark - private methods
#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataSuorce.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ChatListCell *cell = [tableView dequeueReusableCellWithIdentifier:ChatListCellID];
    cell.xdConversationModel = self.dataSuorce[indexPath.row];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return ChatListCellHeight;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ChatViewController *chatViewController = [[ChatViewController alloc] init];
    chatViewController.hidesBottomBarWhenPushed = YES;
    chatViewController.userProfile = self.dataSuorce[indexPath.row].tIMUserProfile;
    [self.navigationController pushViewController:chatViewController animated:YES];
}

#pragma mark - Custom Delegate
#pragma mark - event response
#pragma mark - getters and setters
- (UITableView *)tableView{
    if(!_tableView){
        _tableView = [[UITableView alloc] initWithFrame:self.view.bounds];
        _tableView.tableFooterView = [UIView new];
        [_tableView registerNib:[UINib nibWithNibName:ChatListCellID bundle:nil] forCellReuseIdentifier:ChatListCellID];
        _tableView.delegate = self;
        _tableView.dataSource = self;
    }
    return _tableView;
}

- (NSMutableArray <XdConversationModel *>*)dataSuorce{
    if(!_dataSuorce){
        _dataSuorce = [NSMutableArray array];
    }
    return _dataSuorce;
}


@end
