//
//  ContactListViewController.m
//  XdIM
//
//  Created by Dyw on 2018/11/12.
//  Copyright © 2018 Dyw. All rights reserved.
//

#import "ContactListViewController.h"
#import <ImSDK/ImSDK.h>
#import <IMFriendshipExt/IMFriendshipExt.h>
#import "ToastUtils.h"
#import <ReactiveCocoa.h>
#import "ContactListCell.h"
#import "ChatViewController.h"

@interface ContactListViewController ()
<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSArray <TIMUserProfile *>*dataSuorce;

@end

@implementation ContactListViewController

#pragma mark - life cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupUI];
    
    [self getFriendList];
}

#pragma mark - public methods
#pragma mark - private methods
/* 设置UI */
- (void)setupUI{
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"添加好友" style:UIBarButtonItemStylePlain target:self action:@selector(addFriendButtonClick)];
    
    [self.view addSubview:self.tableView];
}

/* 获取好友列表 */
- (void)getFriendList{
    [ToastUtils showHUD];
    @weakify(self);
    [[TIMFriendshipManager sharedInstance] getFriendList:^(NSArray *friends) {
        [ToastUtils hiddHUD];
        @strongify(self);
        self.dataSuorce = friends;
        [self.tableView reloadData];
        NSLog(@"-->%@", friends);
    } fail:^(int code, NSString *msg) {
        [ToastUtils hiddHUD];
        [ToastUtils showToast:msg];
        NSLog(@"code->%d  msg->%@", code, msg);
        
    }];
}

/* 添加好友 */
- (void)addFriendWithName:(NSString *)name{
    TIMAddFriendRequest *addFriendRequest = [[TIMAddFriendRequest alloc] init];
    addFriendRequest.identifier = name;
    [ToastUtils showHUD];
    @weakify(self);
    [[TIMFriendshipManager sharedInstance] addFriend:@[addFriendRequest] succ:^(NSArray *friends) {
        @strongify(self);
        [ToastUtils hiddHUD];
        NSLog(@"-->%@", friends);
        [self getFriendList];
    } fail:^(int code, NSString *msg) {
        [ToastUtils hiddHUD];
        [ToastUtils showToast:msg];
        NSLog(@"code->%d  msg->%@", code, msg);
    }];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataSuorce.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ContactListCell *cell = [tableView dequeueReusableCellWithIdentifier:ContactListCellID];
    cell.userProfile = self.dataSuorce[indexPath.row];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return ContactListCellHeight;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    ChatViewController *chatViewController = [[ChatViewController alloc] init];
    chatViewController.hidesBottomBarWhenPushed = YES;
    chatViewController.userProfile = self.dataSuorce[indexPath.row];
    [self.navigationController pushViewController:chatViewController animated:YES];
}

#pragma mark - Custom Delegate
#pragma mark - event response
/* 添加好友按钮点击 */
- (void)addFriendButtonClick{
    UIAlertController *altVC = [UIAlertController alertControllerWithTitle:nil message:@"添加好友" preferredStyle:UIAlertControllerStyleAlert];
    __block UITextField *inputTF = nil;
    [altVC addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        inputTF = textField;
    }];
    [altVC addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil]];
    @weakify(self);
    [altVC addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        @strongify(self);
        [self addFriendWithName:inputTF.text];
    }]];
    [self presentViewController:altVC animated:YES completion:nil];
}

#pragma mark - getters and setters
/* 懒加载tableview */
- (UITableView *)tableView{
    if(!_tableView){
        _tableView = [[UITableView alloc] initWithFrame:self.view.bounds];
        _tableView.tableFooterView = [UIView new];
        [_tableView registerNib:[UINib nibWithNibName:ContactListCellID bundle:nil] forCellReuseIdentifier:ContactListCellID];
        _tableView.delegate = self;
        _tableView.dataSource = self;
    }
    return _tableView;
}

@end
