//
//  ChatViewController.m
//  XdIM
//
//  Created by Dyw on 2018/11/12.
//  Copyright © 2018 Dyw. All rights reserved.
//

#import "ChatViewController.h"
#import <IMMessageExt/IMMessageExt.h>
#import "ToastUtils.h"

@interface ChatViewController ()

/* 会话对象 */
@property (nonatomic, strong) TIMConversation *tIMConversation;
/* 数据源 */
@property(nonatomic, strong) NSMutableArray <TIMMessage *>* dataSource;

@end

@implementation ChatViewController

#pragma mark - life cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    /* 获取会话 */
    [self getConversation];
    /* 获取最近消息列表 */
    [self getLastMessageList];
}

#pragma mark - public methods
#pragma mark - private methods
/* 获取会话 */
- (void)getConversation{
    self.title = self.userProfile.nickname.length?self.userProfile.nickname:self.userProfile.identifier;
    self.tIMConversation = [[TIMManager sharedInstance] getConversation:TIM_C2C receiver:self.userProfile.identifier];
}

/* 获取最近消息列表 */
- (void)getLastMessageList{
    [ToastUtils showHUD];
    [self.tIMConversation getMessage:10 last:nil succ:^(NSArray *msgs) {
        [ToastUtils hiddHUD];
        [self.dataSource removeAllObjects];
        [self.dataSource addObjectsFromArray:msgs];
        
        NSLog(@"初次获取到好友：%@", msgs);
        
    } fail:^(int code, NSString *msg) {
        [ToastUtils hiddHUD];
        [ToastUtils showToast:msg];
    }];
}

/* 发送消息 */
- (void)sendMessge:(NSString *)message{
    TIMTextElem *textElem = [[TIMTextElem alloc] init];
    textElem.text = message;
    TIMMessage *tIMMessage = [[TIMMessage alloc] init];
    [tIMMessage addElem:textElem];
    
    [self.tIMConversation sendMessage:tIMMessage succ:^{
        NSLog(@"发送消息成功");
    } fail:^(int code, NSString *msg) {
        NSLog(@"发送失败->%@", msg);
    }];
    
}

#pragma mark - UITableViewDataSource
#pragma mark - UITableViewDelegate
#pragma mark - Custom Delegate
#pragma mark - event response
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self sendMessge:@"这个是消息测试"];
}

#pragma mark - getters and setters
- (NSMutableArray <TIMMessage *>*)dataSource{
    if(!_dataSource){
        _dataSource = [NSMutableArray array];
    }
    return _dataSource;
}

@end
