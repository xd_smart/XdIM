//
//  ChatViewController.h
//  XdIM
//
//  Created by Dyw on 2018/11/12.
//  Copyright © 2018 Dyw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ImSDK/ImSDK.h>

NS_ASSUME_NONNULL_BEGIN

@interface ChatViewController : UIViewController

/* 用户 */
@property (nonatomic, strong) TIMUserProfile *userProfile;

@end

NS_ASSUME_NONNULL_END
