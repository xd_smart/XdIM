//
//  ContactListCell.h
//  XdIM
//
//  Created by Dyw on 2018/11/12.
//  Copyright © 2018 Dyw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <IMFriendshipExt/IMFriendshipExt.h>

#define ContactListCellID @"ContactListCell"
#define ContactListCellHeight 78

NS_ASSUME_NONNULL_BEGIN

@interface ContactListCell : UITableViewCell

@property (nonatomic, strong) TIMUserProfile *userProfile;

@end

NS_ASSUME_NONNULL_END
