//
//  ContactListCell.m
//  XdIM
//
//  Created by Dyw on 2018/11/12.
//  Copyright © 2018 Dyw. All rights reserved.
//

#import "ContactListCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface ContactListCell ()

@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *idLabel;


@end

@implementation ContactListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setUserProfile:(TIMUserProfile *)userProfile{
    _userProfile = userProfile;
    [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:_userProfile.faceURL]];
    self.nameLabel.text = _userProfile.nickname?:@"还没取名字咧";
    self.idLabel.text = _userProfile.identifier;
}

@end
