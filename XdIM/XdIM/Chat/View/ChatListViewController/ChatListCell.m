//
//  ChatListCell.m
//  XdIM
//
//  Created by Dyw on 2018/11/13.
//  Copyright © 2018 Dyw. All rights reserved.
//

#import "ChatListCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <IMMessageExt/IMMessageExt.h>

@interface ChatListCell ()

@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *lastMsgLabel;

@end

@implementation ChatListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setXdConversationModel:(XdConversationModel *)xdConversationModel{
    _xdConversationModel = xdConversationModel;
    
    self.nameLabel.text = _xdConversationModel.tIMUserProfile.nickname.length?_xdConversationModel.tIMUserProfile.nickname:_xdConversationModel.tIMUserProfile.identifier;
    [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:_xdConversationModel.tIMUserProfile.faceURL]];
    
    NSArray <TIMMessage *>*lastMessageArr = [_xdConversationModel.tIMConversation getLastMsgs:1];
    if(lastMessageArr.count && [lastMessageArr.firstObject elemCount]){
        id obj = [lastMessageArr.firstObject getElem:0];
        if([obj isKindOfClass:[TIMTextElem class]]){
            TIMTextElem *tIMTextElem = (TIMTextElem *)obj;
            self.lastMsgLabel.text = tIMTextElem.text;
        }
    }
}


@end
