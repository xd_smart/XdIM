//
//  ChatListCell.h
//  XdIM
//
//  Created by Dyw on 2018/11/13.
//  Copyright © 2018 Dyw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ImSDK/ImSDK.h>
#import "XdConversationModel.h"

#define ChatListCellID @"ChatListCell"
#define ChatListCellHeight 70

NS_ASSUME_NONNULL_BEGIN

@interface ChatListCell : UITableViewCell

@property (nonatomic, strong) XdConversationModel *xdConversationModel;

@end

NS_ASSUME_NONNULL_END
